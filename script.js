// Déclaration des variables globales;
let strPseudo;
let intBestScore=0;



// Fonctions;
/** Fonction permettant de demander un pseudonyme au joueur.
* @author Ronan
* @returns {boolean} controle d'erreur
**/
function demandePseudo(){
    do{
        strPseudo=prompt("Saisissez votre Pseudo");         // Saisie du pseudonyme par le joueur;
    } while(strPseudo==="");                                // Contrôle de saisie;

    if (strPseudo!==""){
        return true;
    }
    return false;
}

/** Fonction retournant le nombre de voyelles d'un mot.
* @author Ronan
* @param pMot {string} mot testé
* @returns {integer} nombre de voyelles
**/
function compteVoyellles(pMot){
    let intNbVoyelle=0;

    for(let i=0;i<pMot.length;i++){
        if(pMot[i]==="a"||pMot[i]==="e"||pMot[i]==="i"||pMot[i]==="o"||pMot[i]==="u"||pMot[i]==="y"){           // Test si chaque lettre du mot est un voyelle;
            intNbVoyelle++;                                                                                     // Incrémente le nombre de voyelles;
        }
    }

    return intNbVoyelle;
}

/** Fonction retournant de façon aléatoire pile ou face.
* @author Ronan
* @returns {integer} nombre symbolisant pile ou face
**/
function pileOuFace(){
    return Math.round(Math.random());               // Génère aléatoirement 1 ou 0;

}

/** Fonction demandant pile ou face au joueur et indiquant si il a gagné.
* @author Ronan
* @returns {boolean} manche gagnée ou non
**/
function lancerPileOuFace(){
    let intSaisieJoueur;

    do{
        intSaisieJoueur=parseInt(prompt("Pile (saisir 1) ou Face (saisir 0) ?"))            // Saisie de la prédiction du joueur (Pile ou Face);
    } while(intSaisieJoueur!==1&&intSaisieJoueur!==0);                                      // Contrôle de saisie;

    if(intSaisieJoueur===pileOuFace()){                                                     // Test si le joueur à gagné la manche ou non;
        return true;
    } else{
        return false;
    }
}

/** Fonction gérant la partie en cours.
* @author Ronan
**/
function jeuPileOuFace(){
    let intScore=0;
    let intChances=compteVoyellles(strPseudo);        // Initialise son nombre de "Chances" en fonction du nombre de voyelles de son pseudonyme;

    while(intChances>0){                              // Le joueur joue tant qu'il lui reste des "Chances";
        if(lancerPileOuFace()){                       // Lancement d'un manche. Si TRUE -> Score++ Else -> Chances--;
            intScore++;
            console.log("Vous avez gagné la manche!\nVotre score : "+intScore+"\nChance(s) restante(s) : "+intChances);
        } else{
            intChances--;
            console.log("Vous avez perdu la manche...\nVotre score : "+intScore+"\nChance(s) restante(s) : "+intChances);
        }
    }

    if(intScore>intBestScore){                        // Test si le meilleur score a été battu;
        intBestScore=intScore;
    }
    console.log("La partie est terminée.\nVotre score : "+intScore+"\nMeilleur score : "+strPseudo+" - "+intBestScore+"pts");
}



// "Executable";
/** Fonction permettant de lancer le programme.
* @author Ronan
* @returns {boolean} controle d'erreur
**/
function main(){
    let boolError=true;
    let strRejouer;

    demandePseudo();                      // Demande le pseudonyme au joueur;
    
    do{
        jeuPileOuFace();                  // Lance la partie;
        do{
            strRejouer=prompt("Souhaitez-vous rejouer ? (o/n)").toLowerCase();          // Demande si le joueur souhaite rejouer;
        } while(strRejouer!=="o"&&strRejouer!=="n");                                    // Contrôle de saisie;
    } while(strRejouer==="o");

    // A voir comment intégrer ce point (4) dans le programme; (Enfin la c'est fonctionnel mais inutile);
    if(boolError){
        return true;            // Cas : pas d'erreur;
    } else{
        return false;           // Cas : erreur;
    }
}